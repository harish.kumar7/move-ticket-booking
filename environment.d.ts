import { Bus, ReqUser, Ticket, Trip } from './interface'

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            PORT: string
            MONGO_URI: string
            JWT_SECRET: string
        }
    }
    namespace Express {
        export interface Request {
            user?: ReqUser
            bus?: Bus
            trip?: Document<any, any, any> & Trip
            ticket?: Document<any, any, any> & Ticket
        }
    }
}

export {}
