import { Router } from 'express'
import { body, param } from 'express-validator'
import { isAuthorised } from '../../middleware/auth.middleware'
import { isValidRegistration } from '../../utils/validation/busNo'
import {
    deleteBus,
    getAllBuses,
    getBusById,
    putBus,
    postBus,
} from '../../controllers/bus.controller'
import checkObjectIdMiddleware from '../../middleware/checkObjectId.middleware'
import {
    canModifyBus,
    checkBusType,
    isBusNoExists,
} from '../../middleware/bus.middleware'
import wrapAsync from '../../utils/wrapAsnyc'

const busRouter = Router()

/**
 *@route    POST api/bus
 *@desc     Register bus
 *@access   Public
 */
busRouter.post(
    '/',
    isAuthorised('Operator'),
    body('busNo', 'specify registration no of bus').custom(isValidRegistration),
    body('ac', 'specify whether bus is ac or not').isBoolean(),
    body('type', 'specify type of bus').notEmpty(),
    body('type').custom(checkBusType),
    wrapAsync(isBusNoExists),
    wrapAsync(postBus)
    /**
     * #swagger.summary = 'register new bus'
     * #swagger.description = 'register new bus by providing details'
     * #swagger.tags = ['bus']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['requestBody'] = {
                in: 'body',
                description: 'bus details required for registration',
                schema: { $ref: '#/definitions/bus' },
            required: true
        } 
        * #swagger.responses[201] = {
            description: 'bus created successfully, ',
            schema: { $ref: '#/definitions/id' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    GET api/bus
@desc     get all buses of operator
@access   Public
*/
busRouter.get(
    '/',
    isAuthorised('Operator'),
    wrapAsync(getAllBuses)
    /**
     * #swagger.summary = 'get all bus details'
     * #swagger.description = 'get all bus details registered under you'
     * #swagger.tags = ['bus']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
     * #swagger.responses[200] = {
            description: 'list of buses',
            schema: { $ref: '#/definitions/id' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'no buses found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    GET api/bus/:_id
@desc     get specific bus given id
@access   Public
*/
busRouter.get(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', 'id of bus being requested required').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(getBusById)
    /**
     * #swagger.summary = 'get bus details'
     * #swagger.description = 'get bus details from id'
     * #swagger.tags = ['bus']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['_id'] = {
                in: 'path',
                description: 'bus id for which details queried',
                schema: { $ref: '#/definitions/id' },
            required: true
        } 
        * #swagger.responses[200] = {
            description: 'bus details',
            schema: { $ref: '#/definitions/id' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'bus not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    PUT api/bus
@desc     update bus
@access   Public
*/
busRouter.put(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', 'id of bus being deleted required').notEmpty(),
    checkObjectIdMiddleware('_id'),
    body('ac', 'specify whether bus is ac or not').isBoolean(),
    body('type', 'specify type of bus').notEmpty(),
    body('type').custom(checkBusType),
    wrapAsync(canModifyBus),
    wrapAsync(isBusNoExists),
    wrapAsync(putBus)
    /**
     * #swagger.summary = 'update bus'
     * #swagger.description = 'update bus details if possible'
     * #swagger.tags = ['bus']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['_id'] = {
            in: 'path',
            description: 'bus id to be modified',
            schema: { $ref: '#/definitions/id' }
        } 
        #swagger.parameters['requestBody'] = {
            in: 'body',
            description: 'bus details required for updation',
            schema: { $ref: '#/definitions/bus' },
            required: true
        } 
        * #swagger.responses[204] = {
            description: 'update successfull'
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'Bad Request: resource cannot be modified',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[404] = {
                description: 'no buses found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    delete api/bus/:_id
@desc     delete bus given id
@access   Public
*/
busRouter.delete(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', '_id of bus being deleted required').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(canModifyBus),
    wrapAsync(deleteBus)
    /**
     * #swagger.summary = 'delete bus by id'
     * #swagger.description = 'delete bus by id if it belongs to you and no trips in future'
     * #swagger.tags = ['bus']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['_id'] = {
                in: 'path',
                description: 'bus id to be deleted',
                schema: { $ref: '#/definitions/id' }
        } 
    * #swagger.responses[204] = {
            description: 'bus deleted successfully'
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'bus not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

export default busRouter
