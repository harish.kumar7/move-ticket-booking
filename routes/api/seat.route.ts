import { Router } from 'express'
import { param } from 'express-validator'
import { getSeatsOfTripById } from '../../controllers/trip.controller'
import { isAuthorised } from '../../middleware/auth.middleware'
import checkObjectIdMiddleware from '../../middleware/checkObjectId.middleware'
import wrapAsync from '../../utils/wrapAsnyc'

const seatRouter = Router()
/**
@route    GET api/seats/{_id}
@desc     search buses
@access   Public
*/
seatRouter.get(
    '/:_id',
    isAuthorised('Customer'),
    param('_id', 'trip id should be string').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(getSeatsOfTripById)
    /**
     * #swagger.summary = 'get booking status from id'
     * #swagger.description = 'get trip seats booking status from id'
     * #swagger.tags = ['search']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
      #swagger.parameters['_id'] = {
                in: 'path',
                description: 'trip id to get seats of',
                schema: { $ref: '#/definitions/id' },
        }   
        * #swagger.responses[200] = {
            description: 'list of seats',
            schema: { $ref: '#/definitions/seatResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'trip not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

export default seatRouter
