import { Router } from 'express'
import { body, param } from 'express-validator'
import {
    postTicket,
    getTicketById,
    getTickets,
    putTicket,
    deleteTicket,
} from '../../controllers/ticket.controller'
import { isAuthorised } from '../../middleware/auth.middleware'
import checkObjectIdMiddleware from '../../middleware/checkObjectId.middleware'
import {
    isValidSeats,
    isValidTicket,
    isValidTrip,
} from '../../middleware/ticket.middleware'
import { checkSeats } from '../../middleware/trip.middleware'
import wrapAsync from '../../utils/wrapAsnyc'

const ticketRouter = Router()

ticketRouter.post(
    '/:tripId',
    isAuthorised('Customer'),
    param('tripId', 'trip id must be present in url params').notEmpty(),
    checkObjectIdMiddleware('tripId'),
    body('seats').custom(checkSeats),
    wrapAsync(isValidTrip),
    isValidSeats,
    wrapAsync(postTicket)
    /**
     * #swagger.summary = 'book ticket'
     * #swagger.description = 'book ticket by registering seats'
     * #swagger.tags = ['ticket']
     * #swagger.security = [{
            "apiKeyAuth": []
        }]  
      #swagger.parameters['requestBody'] = {
            in: 'body',
            description: 'seats to be booked with passenger details',
            schema: { $ref: '#/definitions/ticket' },
            required: true
        }  
        * #swagger.responses[201] = {
            description: 'ticket created',
            schema: { $ref: '#/definitions/id' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

ticketRouter.get(
    '/',
    isAuthorised('Customer'),
    wrapAsync(getTickets)
    /**
     * #swagger.summary = 'get tickets of user'
     * #swagger.description = 'get tickets of user'
     * #swagger.tags = ['ticket']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
     * #swagger.responses[200] = {
            description: 'list of tickets',
            schema: { $ref: '#/definitions/ticketResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'no tickets found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

ticketRouter.get(
    '/:_id',
    isAuthorised('Customer'),
    param('_id', 'ticket id required in url').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(getTicketById)
    /**
     * #swagger.summary = 'get ticket from id'
     * #swagger.description = 'get tickets from id'
     * #swagger.tags = ['ticket']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
    #swagger.parameters['_id'] = {
        in: 'path',
        description: 'ticket id',
        schema: { $ref: '#/definitions/id' },
    }   
     * #swagger.responses[200] = {
            description: 'ticket details',
            schema: { $ref: '#/definitions/ticketResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'ticket not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

ticketRouter.put(
    '/:_id',
    isAuthorised('Customer'),
    param('_id', 'ticket id required in url').notEmpty(),
    checkObjectIdMiddleware('_id'),
    body('seats').custom(checkSeats),
    wrapAsync(isValidTicket),
    wrapAsync(isValidTrip),
    isValidSeats,
    wrapAsync(putTicket)
    /**
     * #swagger.summary = 'update ticket from id'
     * #swagger.description = 'update tickets from id'
     * #swagger.tags = ['ticket']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
    #swagger.parameters['_id'] = {
        in: 'path',
        description: 'ticket id',
        schema: { $ref: '#/definitions/id' },
    }   
    #swagger.parameters['requestBody'] = {
        in: 'body',
        description: 'ticket details',
        schema: { $ref: '#/definitions/ticket'},
        required: true
    }
     * #swagger.responses[204] = {
            description: 'ticket deleted',

        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'fields not proper',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[404] = {
                description: 'ticket not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

ticketRouter.delete(
    '/:_id',
    isAuthorised('Customer'),
    param('_id', 'ticket id required').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(isValidTicket),
    wrapAsync(isValidTrip),
    wrapAsync(deleteTicket)
    /**
     * #swagger.summary = 'delete ticket from id'
     * #swagger.description = 'delete tickets from id'
     * #swagger.tags = ['ticket']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
    #swagger.parameters['_id'] = {
        in: 'path',
        description: 'ticket id',
        schema: { $ref: '#/definitions/id' },
    }   
    * #swagger.responses[204] = {
            description: 'ticket deleted successfully',
            schema: { $ref: '#/definitions/ticketResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'ticket not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

export default ticketRouter
