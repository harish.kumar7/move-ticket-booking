import { Router } from 'express'
import { body, param } from 'express-validator'
import {
    postTrip,
    getTripById,
    getTripsByOperatorOrBus,
    deleteTrip,
    putTrip,
} from '../../controllers/trip.controller'
import { isAuthorised } from '../../middleware/auth.middleware'
import checkObjectIdMiddleware from '../../middleware/checkObjectId.middleware'
import {
    canModifyTrip,
    checkEndAfterStart,
    isValidBus,
} from '../../middleware/trip.middleware'
import wrapAsync from '../../utils/wrapAsnyc'

import { isDateInFuture } from '../../utils/validation/date'

const tripRouter = Router()

/**
@route    POST api/trip
@desc     Register trip for bus
@access   Public
*/
tripRouter.post(
    '/',
    isAuthorised('Operator'),
    body('busId', 'Bus id is required').notEmpty(),
    checkObjectIdMiddleware('busId'),
    body('from', 'from city is required').notEmpty(),
    body('to', 'to city is required').notEmpty(),
    body('startDateTime').custom(isDateInFuture),
    body('endDateTime').custom(checkEndAfterStart),
    body('sleeperPrice', 'sleeper price is required as numeric')
        .notEmpty()
        .isNumeric(),
    body('seaterPrice', 'seater price is required as numeric')
        .notEmpty()
        .isNumeric(),
    body('singlePriceMarkup', 'single price markup is required as numeric')
        .notEmpty()
        .isNumeric(),
    wrapAsync(isValidBus),
    wrapAsync(postTrip)
    /**
     * #swagger.summary = 'register new trip for bus'
     * #swagger.description = 'register new trip by providing details and bus id'
     * #swagger.tags = ['trip']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['requestBody'] = {
            in: 'body',
            description: 'trip details required for registration',
            schema: { $ref: '#/definitions/trip' },
            required: true
        } 
        * #swagger.responses[201] = {
            description: 'trip created successfully, ',
            schema: { $ref: '#/definitions/id' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    GET api/trip
@desc     get all trips of operator or bus
@access   Public*/
tripRouter.get(
    '/',
    isAuthorised('Operator'),
    wrapAsync(getTripsByOperatorOrBus)
    /**
     * #swagger.summary = 'get all trips of operator or bus'
     * #swagger.description = 'get all trips registered by operator or bus based on query'
     * #swagger.tags = ['trip']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['busId'] = {
            in: 'query',
            description: 'bus id to get trips of',
            schema: { $ref: '#/definitions/id' },
        }   
     * #swagger.responses[200] = {
            description: 'list of trips',
            schema: { $ref: '#/definitions/tripResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'no trips found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    GET api/trip/:_id
@desc     get trip by id
@access   Public
*/
tripRouter.get(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', 'trip id required in url params').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(getTripById)
    /**
     * #swagger.summary = 'get trip details from id'
     * #swagger.description = 'get trip details from id'
     * #swagger.tags = ['trip']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
      #swagger.parameters['_id'] = {
                in: 'path',
                description: 'id to get trips details of',
                schema: { $ref: '#/definitions/id' },
        }   
        * #swagger.responses[200] = {
            description: 'trip details',
            schema: { $ref: '#/definitions/tripResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
                description: 'trip not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    PUT api/trip
@desc     Update trip for bus
@access   Public
*/
tripRouter.put(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', 'trip id required in url params').notEmpty(),
    checkObjectIdMiddleware('_id'),
    body('busId', 'Bus id is required').notEmpty(),
    checkObjectIdMiddleware('busId'),
    body('from', 'from city is required').notEmpty(),
    body('to', 'to city is required').notEmpty(),
    body('startDateTime').custom(isDateInFuture),
    body('endDateTime').custom(checkEndAfterStart),
    body('sleeperPrice', 'sleeper price is required as numeric')
        .notEmpty()
        .isNumeric(),
    body('seaterPrice', 'seater price is required as numeric')
        .notEmpty()
        .isNumeric(),
    body('singlePriceMarkup', 'single price markup is required as numeric')
        .notEmpty()
        .isNumeric(),
    wrapAsync(canModifyTrip),
    wrapAsync(isValidBus),
    wrapAsync(putTrip)
    /**
     * #swagger.summary = 'update trip details from id'
     * #swagger.description = 'update trip details from id'
     * #swagger.tags = ['trip']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
      #swagger.parameters['_id'] = {
                in: 'path',
                description: 'trip id to be modified',
                schema: { $ref: '#/definitions/id' },
        }   
        #swagger.parameters['requestBody'] = {
            in: 'body',
            description: 'trip modification details',
            schema: { $ref: '#/definitions/trip' },
            required: true
        } 
        * #swagger.responses[204] = {
            description: 'trip updated successfully',
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[404] = {
                description: 'trip not found',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

/**
@route    DELETE api/trip/:_id
@desc     delete trip by id
@access   Public
*/
tripRouter.delete(
    '/:_id',
    isAuthorised('Operator'),
    param('_id', 'trip id required in url params').notEmpty(),
    checkObjectIdMiddleware('_id'),
    wrapAsync(canModifyTrip),
    wrapAsync(deleteTrip)
    /**
     * #swagger.summary = 'delete trip by id'
     * #swagger.description = 'delete trip by id'
     * #swagger.tags = ['trip']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
      #swagger.parameters['_id'] = {
                in: 'path',
                description: 'trip id to be deleted',
                schema: { $ref: '#/definitions/id' },
        }   
        * #swagger.responses[204] = {
            description: 'trip deleted successfully',
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[404] = {
            description: 'trip not found',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

export default tripRouter
