import { Router } from 'express'
import { query } from 'express-validator'
import { searchTrips } from '../../controllers/trip.controller'
import { isAuthorised } from '../../middleware/auth.middleware'
import wrapAsync from '../../utils/wrapAsnyc'
import { isDateInFuture } from '../../utils/validation/date'

const searchRouter = Router()

/**
@route    POST api/searchTrip
@desc     search buses
@access   Public
*/
searchRouter.get(
    '/',
    isAuthorised('Customer'),
    query('from', 'from parameter required').notEmpty(),
    query('to', 'to parameter required').notEmpty(),
    query('date', 'date parameter required').notEmpty(),
    query('date').custom(isDateInFuture),
    wrapAsync(searchTrips)
    /**
     * #swagger.summary = 'get trips matching query'
     * #swagger.description = 'get available trips based on query'
     * #swagger.tags = ['search']
     * #swagger.security = [{
            "apiKeyAuth": []
        }] 
        #swagger.parameters['from'] = {
            description: 'search params',
            schema: 'madurai',
            required: true
        } 
        #swagger.parameters['to'] = {
            description: 'search params',
            schema: 'coimbatore',
            required: true
        } 
        #swagger.parameters['date'] = {
            description: 'search params',
            schema: 'coimbatore',
            required: true
        } 
        #swagger.parameters['ac'] = {
            description: 'search params',
            schema: true
        } 
        #swagger.parameters['seater'] = {
            description: 'search params',
            schema: true
        } 
        #swagger.parameters['sleeper'] = {
            description: 'search params',
            schema: true
        } 
        * #swagger.responses[200] = {
            description: 'list of trips',
            schema: { $ref: '#/definitions/tripResponse' }
        }
        * #swagger.responses[401] = {
            description: 'unauthorized or token invalid or no token in header',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[403] = {
            description: 'Forbidden: not enough privileges',
            schema: { $ref: '#/definitions/validationError' }
        }
        * #swagger.responses[400] = {
                description: 'validation error, ensure fields are correct',
                schema: { $ref: '#/definitions/validationError' }
            }
        * #swagger.responses[503] = {
                description: 'database error',
                schema: { $ref: '#/definitions/customError' }
            }
        * #swagger.responses[500] = {
                description: 'internal error',
                schema: { $ref: '#/definitions/customError' }
            }
    */
)

export default searchRouter
