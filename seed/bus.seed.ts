export const busSleeperSeaterAc = {
    busNo: 'TN58V5464',
    ac: true,
    type: 'SeaterSleeper',
}

export const busSeater = {
    busNo: 'TN58V5465',
    ac: false,
    type: 'Seater',
}

export const busSleeper = {
    busNo: 'TN58V5466',
    ac: false,
    type: 'Sleeper',
}

export const registerBus = {
    busNo: 'TN58V5467',
    ac: true,
    type: 'SeaterSleeper',
}
