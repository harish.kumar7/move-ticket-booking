export const ticketSeed = {
    seats: [
        {
            seatNo: 'L8',
            passenger: {
                name: 'Murugappan M',
                age: 20,
                gender: 'Male',
            },
        },
        {
            seatNo: 'L9',
            passenger: {
                name: 'Alagu',
                age: 25,
                gender: 'Female',
            },
        },
    ],
}

export const ticketRegisterSeedPost = {
    seats: [
        {
            seatNo: 'L1',
            passenger: {
                name: 'Alagu',
                age: 25,
                gender: 'Female',
            },
        },
    ],
}

export const ticketSeedSameSeat = {
    seats: [
        {
            seatNo: 'L8',
            passenger: {
                name: 'Alagu',
                age: 25,
                gender: 'Female',
            },
        },
    ],
}

export const ticketRegisterSeedPut = {
    seats: [
        {
            seatNo: 'L6',
            passenger: {
                name: 'Murugappan M',
                age: 20,
                gender: 'Male',
            },
        },
    ],
}

export const invalidTicketSeed = {
    seats: [
        {
            seatNo: 'Lss6',
            passenger: {
                name: 'Murugappan M',
                age: 20,
                gender: 'Male',
            },
        },
    ],
}

export const repeatedSeatsRequest = {
    seats: [
        {
            seatNo: 'L5',
            passenger: {
                name: 'Murugappan M',
                age: 20,
                gender: 'Male',
            },
        },
        {
            seatNo: 'L5',
            passenger: {
                name: 'Alagu',
                age: 25,
                gender: 'Female',
            },
        },
    ],
}
