export const trip = {
    from: 'madurai',
    to: 'coimbatore',
    startDateTime: '2025-10-21T10:08:54Z',
    endDateTime: '2025-10-21T16:08:54Z',
    sleeperPrice: 500,
    seaterPrice: 400,
    singlePriceMarkup: 50,
}

export const updateTripSeed = {
    from: 'coimbatore',
    to: 'madurai',
    startDateTime: '2025-10-22T10:08:54Z',
    endDateTime: '2025-10-22T16:08:54Z',
    sleeperPrice: 600,
    seaterPrice: 500,
    singlePriceMarkup: 60,
}
