/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
    testEnvironment: 'node',
    testPathIgnorePatterns: ['.js'],
    modulePathIgnorePatterns: ['common'],
    collectCoverageFrom: ['**/*.ts', '!**/node_modules/**', '!**/*.d.ts'],
    coverageReporters: ['html', 'text', 'text-summary', 'cobertura'],
    testTimeout: 60000,
}
