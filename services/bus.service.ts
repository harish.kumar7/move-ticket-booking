import { Types } from 'mongoose'
import { BusModel } from '../models/bus.model'

export const getBusOfNo = async (busNo: string) => {
    return await BusModel.findOne({ busNo })
}

export const createBus = async (
    operatorId: string,
    busNo: string,
    ac: boolean,
    type: string
) => {
    const bus = new BusModel({
        operatorId: new Types.ObjectId(operatorId),
        busNo,
        ac,
        type,
    })
    await bus.save()
    return bus
}

export const getBusOfIds = async (_id: string, operatorId: string) => {
    return await BusModel.findOne({
        _id,
        operatorId,
    }).select('_id busNo ac operatorId type')
}

export const getAllBusesOfOperator = async (operatorId: string) => {
    return await BusModel.find({
        operatorId: operatorId,
    }).select('_id busNo ac operatorId type')
}

export const deleteBusById = async (_id: string) => {
    await BusModel.findByIdAndDelete(_id)
}

export const updateBus = async (
    _id: string,
    busNo: string,
    ac: boolean,
    type: string
) => {
    await BusModel.findByIdAndUpdate(_id, { busNo, ac, type })
}
