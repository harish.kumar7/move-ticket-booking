import { Document, FilterQuery, Types } from 'mongoose'
import { ReqSeat, Ticket, TicketSeat, Trip } from '../interface'
import { TicketModel } from '../models/ticket.model'
import { TripModel } from '../models/trip.model'

export const genTicketSeatDetails = (seats: ReqSeat[], trip: Trip) => {
    const saveSeats: TicketSeat[] = []
    for (const requestSeat of seats) {
        const curSeat = trip.seats.find(
            (seat) => seat.seatNo === requestSeat.seatNo
        )
        if (curSeat)
            saveSeats.push({
                ...curSeat,
                passenger: requestSeat.passenger,
            })
    }
    return saveSeats
}

export const createTicket = (
    seats: ReqSeat[],
    trip: Document & Trip,
    userId: string
) => {
    return new TicketModel({
        userId: new Types.ObjectId(userId),
        seats: genTicketSeatDetails(seats, trip),
    })
}

export const bookTicket = async (tripId: Types.ObjectId, ticket: Ticket) => {
    const reqSeatNos = ticket.seats.map((seat) => seat.seatNo)
    return await TripModel.findOneAndUpdate(
        {
            _id: tripId,
            availableSeats: { $all: reqSeatNos },
        },
        {
            $pull: { availableSeats: { $in: reqSeatNos } },
            $push: { tickets: ticket },
        }
    )
}

export const updateTicket = async (
    ticket: Ticket,
    trip: Trip,
    newSeats: ReqSeat[]
) => {
    // seats to be booked from new ticket
    const newSeatNos = newSeats.map((seat) => seat.seatNo)
    // seats to be freed/deleted from old ticket
    const oldSeatNos = ticket.seats.map((seat) => seat.seatNo)
    // seats in new ticket and not in old ticket that we should check for availability
    const requiredSeatNos = newSeatNos.filter(
        (seatNo) => !oldSeatNos.includes(seatNo)
    )
    // available seats after updating ticket
    const availableSeats = trip.availableSeats
        .concat(oldSeatNos)
        .filter((seatNo) => !newSeatNos.includes(seatNo))
    const modSeats = genTicketSeatDetails(newSeats, trip)
    const filter: FilterQuery<Trip> = { 'tickets._id': ticket._id }
    if (requiredSeatNos.length !== 0)
        filter.availableSeats = { $all: requiredSeatNos }
    return await TripModel.findOneAndUpdate(filter, {
        $set: {
            availableSeats,
            'tickets.$.seats': modSeats,
        },
    })
}

const getTicketsFromTripQuery = (userId: string) => {
    return [
        { $match: { 'tickets.userId': new Types.ObjectId(userId) } },
        { $unwind: '$tickets' },
        { $match: { 'tickets.userId': new Types.ObjectId(userId) } },
        {
            $project: {
                _id: '$tickets._id',
                tripId: '$_id',
                bus: 1,
                from: 1,
                to: 1,
                startDateTime: 1,
                endDateTime: 1,
                seats: '$tickets.seats',
            },
        },
    ]
}

export const getAllTicketsOfId = async (userId: string) => {
    return await TripModel.aggregate(getTicketsFromTripQuery(userId))
}

export const getTicketOfIds = async (_id: string, userId: string) => {
    const tickets = await TripModel.aggregate([
        ...getTicketsFromTripQuery(userId),
        {
            $match: { _id: new Types.ObjectId(_id) },
        },
    ])
    if (tickets.length === 1) return tickets[0]
    return null
}

export const deleteTicketOfId = async (ticket: Ticket) => {
    await TripModel.findOneAndUpdate(
        {
            'tickets._id': ticket._id,
        },
        {
            $pull: { tickets: { _id: ticket._id } },
            $push: {
                availableSeats: {
                    $each: ticket.seats.map((seat) => seat.seatNo),
                },
            },
        }
    )
}
