import express, { Response, NextFunction } from 'express'
import { CustomValidator } from 'express-validator'
import { busError, errorWrapper, userError } from '../errorResponse'
import { getBusOfIds, getBusOfNo } from '../services/bus.service'
import { getTripInFutureOfBus } from '../services/trip.service'
import { BAD_REQUEST, NOT_FOUND } from '../statusCodes'

export const checkBusType: CustomValidator = (type: string) => {
    if (!['Sleeper', 'Seater', 'SeaterSleeper'].includes(type))
        throw new Error(busError.type)
    return true
}

export const isBusNoExists = async (
    req: express.Request,
    res: Response,
    next: NextFunction
) => {
    const { busNo } = req.body
    if (await getBusOfNo(busNo)) {
        return res
            .status(BAD_REQUEST)
            .send(errorWrapper(busError.registerNoRepeated))
    }
    next()
}

export const canModifyBus = async (
    req: express.Request,
    res: Response,
    next: NextFunction
) => {
    const _id = req.params._id
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const bus = await getBusOfIds(_id, operatorId)
    if (!bus) {
        return res.status(NOT_FOUND).send(errorWrapper(busError.notFound))
    }
    const trip = await getTripInFutureOfBus(bus._id.toString(), operatorId)
    if (trip) {
        return res.status(BAD_REQUEST).send(errorWrapper(busError.tripInFuture))
    }
    next()
}
