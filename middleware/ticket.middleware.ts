import { NextFunction, Request, Response } from 'express'
import { Types } from 'mongoose'
import {
    errorWrapper,
    ticketError,
    tripError,
    userError,
} from '../errorResponse'
import { Trip } from '../interface'
import { getTicketOfIds } from '../services/ticket.service'
import { getTripOfId, isTripInPast } from '../services/trip.service'
import { BAD_REQUEST, NOT_FOUND } from '../statusCodes'

export const isValidTrip = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const tripId = req.ticket?.tripId || new Types.ObjectId(req.params.tripId)
    const trip = await getTripOfId(tripId)
    if (!trip) {
        return res.status(NOT_FOUND).send(errorWrapper(tripError.notFound))
    }
    if (isTripInPast(trip)) {
        return res.status(BAD_REQUEST).send(errorWrapper(tripError.inPast))
    }
    req.trip = trip
    next()
}

export const isValidSeats = (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { seats } = req.body
    const trip: Trip = req.trip
    if (!trip) throw new Error(tripError.notDefined)
    const allSeats = new Set(trip.seats.map((seat) => seat.seatNo))
    for (const requestSeat of seats) {
        const curSeatNo = requestSeat.seatNo
        if (!allSeats.has(curSeatNo)) {
            return res
                .status(BAD_REQUEST)
                .send(errorWrapper(ticketError.seatNotFound(curSeatNo)))
        }
    }
    next()
}

export const isValidTicket = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    const { _id } = req.params
    const userId = req.user?._id
    if (!userId) throw new Error(userError.notDefined)
    const ticket = await getTicketOfIds(_id, userId)
    if (!ticket) {
        return res.status(NOT_FOUND).send(errorWrapper(ticketError.notFound))
    }
    req.ticket = ticket
    next()
}
