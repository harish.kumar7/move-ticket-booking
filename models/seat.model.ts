import { Schema } from 'mongoose'
import { Seat } from '../interface'

export const seatSchema = new Schema<Seat>({
    seatNo: {
        type: String,
        required: true,
    },
    type: {
        sleeper: {
            type: Boolean,
            required: true,
        },
        upper: {
            type: Boolean,
            required: true,
        },
        single: {
            type: Boolean,
            required: true,
        },
    },
    price: {
        type: Number,
        required: true,
        min: 10,
    },
})
