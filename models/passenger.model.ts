import { model, Schema } from 'mongoose'
import { Passenger } from '../interface'

export const passengerSchema = new Schema<Passenger>({
    name: {
        type: String,
        required: true,
    },
    gender: {
        type: String,
        required: true,
        enum: ['Male', 'Female', 'Other'],
    },
    age: {
        type: Number,
        required: true,
        min: 0,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} is not an integer value',
        },
    },
})

export const PassengerModel = model<Passenger>('Passenger', passengerSchema)
