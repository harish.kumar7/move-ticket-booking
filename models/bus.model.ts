import { Schema, model } from 'mongoose'
import { Bus } from '../interface'
import { UserModel } from './user.model'

export const busSchema = new Schema<Bus>({
    operatorId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: UserModel,
    },
    busNo: {
        type: String,
        required: true,
        validate: /^[A-Z]{2}\d{2}[A-Z]{1,2}\d{4}$/,
    },
    type: {
        type: String,
        required: true,
        enum: ['Sleeper', 'Seater', 'SeaterSleeper'],
    },
    ac: {
        type: Boolean,
        required: true,
    },
    creationDate: {
        type: Date,
        default: Date.now,
    },
})

export const BusModel = model<Bus>('Bus', busSchema)
