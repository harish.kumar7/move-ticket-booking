import { model, Schema } from 'mongoose'
import { TicketSeat, Ticket } from '../interface'
import { PassengerModel, passengerSchema } from './passenger.model'
import { seatSchema } from './seat.model'
import { UserModel } from './user.model'

export const bookedSeatSchema = new Schema<TicketSeat>({
    ...seatSchema.obj,
    passenger: {
        type: passengerSchema,
        ref: PassengerModel,
    },
})

export const ticketSchema = new Schema<Ticket>({
    userId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: UserModel,
    },
    seats: {
        type: [bookedSeatSchema],
        required: true,
        validate: {
            validator: (seats: TicketSeat[]) => seats.length !== 0,
            message: 'seats must not be empty',
        },
    },
})

export const TicketModel = model<Ticket>('Ticket', ticketSchema)
