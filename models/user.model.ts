import { Schema, model } from 'mongoose'
import { User } from '../interface'

const userSchema = new Schema<User>({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    joiningDate: {
        type: Date,
        default: Date.now,
    },
    type: {
        type: String,
        enum: ['Customer', 'Operator'],
    },
})

export const UserModel = model<User>('User', userSchema)
