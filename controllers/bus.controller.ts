import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { busError, errorWrapper, userError } from '../errorResponse'
import {
    createBus,
    deleteBusById,
    getAllBusesOfOperator,
    getBusOfIds,
    updateBus,
} from '../services/bus.service'
import { CREATION_SUCCESSFULL, NOT_FOUND, NO_CONTENT, OK } from '../statusCodes'

export const postBus = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { ac, type, busNo } = req.body
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const bus = await createBus(operatorId, busNo, ac, type)
    res.status(CREATION_SUCCESSFULL).send(bus._id)
}

export const getAllBuses = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const buses = await getAllBusesOfOperator(operatorId)
    if (buses.length === 0) {
        return res.status(NOT_FOUND).send(errorWrapper(busError.empty))
    }
    res.status(OK).send(buses)
}

export const getBusById = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { _id } = req.params,
        operatorId = req.user?._id
    if (!operatorId) throw new Error(userError.notDefined)
    const bus = await getBusOfIds(_id, operatorId)
    if (!bus) {
        return res.status(NOT_FOUND).send(errorWrapper(busError.notFound))
    }
    res.status(OK).send(bus)
}

export const putBus = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { busNo, ac, type } = req.body
    const _id = req.params._id
    await updateBus(_id, busNo, ac, type)
    res.status(NO_CONTENT).send()
}

export const deleteBus = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const _id = req.params._id
    await deleteBusById(_id)
    res.status(NO_CONTENT).send()
}
