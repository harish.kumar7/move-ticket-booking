import { Request, Response } from 'express'
import { validationResult } from 'express-validator'
import {
    bookTicket,
    createTicket,
    deleteTicketOfId,
    getAllTicketsOfId,
    getTicketOfIds,
    updateTicket,
} from '../services/ticket.service'
import {
    BAD_REQUEST,
    CREATION_SUCCESSFULL,
    NOT_FOUND,
    NO_CONTENT,
    OK,
} from '../statusCodes'
import {
    errorWrapper,
    ticketError,
    tripError,
    userError,
} from '../errorResponse'
import { ReqSeat, Ticket } from '../interface'

export const postTicket = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { seats } = req.body
    const userId = req.user?._id
    if (!userId) throw new Error(userError.notDefined)
    const trip = req.trip
    if (!trip) throw new Error(tripError.notDefined)
    const ticket = createTicket(seats, trip, userId)
    const updatedTrip = await bookTicket(trip._id, ticket)
    if (!updatedTrip) {
        res.status(BAD_REQUEST).send(errorWrapper(ticketError.booked))
    }
    res.status(CREATION_SUCCESSFULL).send(ticket._id.toString())
}

export const getTickets = async (req: Request, res: Response) => {
    const userId = req.user?._id
    if (!userId) throw new Error(userError.notDefined)
    const response = await getAllTicketsOfId(userId)
    if (response.length === 0) {
        return res.status(NOT_FOUND).send(errorWrapper(ticketError.empty))
    }
    res.status(OK).send(response)
}

export const getTicketById = async (req: Request, res: Response) => {
    const userId = req.user?._id
    if (!userId) throw new Error(userError.notDefined)
    const { _id } = req.params
    const ticket = await getTicketOfIds(_id, userId)
    if (!ticket) {
        return res.status(NOT_FOUND).send(errorWrapper(ticketError.notFound))
    }
    return res.status(OK).send(ticket)
}

export const putTicket = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const { seats }: { seats: ReqSeat[] } = req.body
    const ticket: Ticket = req.ticket
    if (!ticket) throw new Error(ticketError.notDefined)
    const trip = req.trip
    if (!trip) throw new Error(tripError.notDefined)
    const updatedTrip = await updateTicket(ticket, trip, seats)
    if (!updatedTrip) {
        res.status(BAD_REQUEST).send(errorWrapper(ticketError.booked))
    }
    res.status(NO_CONTENT).send()
}

export const deleteTicket = async (req: Request, res: Response) => {
    validationResult(req).throw()
    const ticket = req.ticket
    if (!ticket) throw new Error(ticketError.notDefined)
    await deleteTicketOfId(ticket)
    res.status(NO_CONTENT).send()
}
