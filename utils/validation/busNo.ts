export const isValidRegistration = (busNo: string) => {
    if (!/^[A-Z]{2}\d{2}[A-Z]{1,2}\d{4}$/.test(busNo))
        throw new Error('Bus registration number must be in correct format')
    return true
}
