import { readFile } from 'fs/promises'
import { BusSeat, Seat } from '../interface'

export default async (
    type: string,
    sleeperPrice: number,
    seaterPrice: number,
    singlePriceMarkup: number
): Promise<Seat[]> => {
    const file: { [key: string]: BusSeat[] } = JSON.parse(
        await readFile('./utils/seats.json', 'utf-8')
    )
    return file[type].map((seat: BusSeat) => ({
        ...seat,
        price: seat.type.sleeper
            ? sleeperPrice
            : seaterPrice + (seat.type.single ? singlePriceMarkup : 0),
    }))
}
