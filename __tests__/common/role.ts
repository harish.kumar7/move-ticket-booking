import { Response } from 'supertest'
import { idError, tokenError } from '../../errorResponse'
import { FORBIDDEN_REQUEST, BAD_REQUEST } from '../../statusCodes'

export const wrongRole = (res: Response) => {
    expect(res.status).toEqual(FORBIDDEN_REQUEST)
    expect(res.body.errors[0].msg).toEqual(tokenError.notAuthorised)
}

export const wrongId = (res: Response) => {
    expect(res.status).toEqual(BAD_REQUEST)
    expect(res.body.errors[0].msg).toEqual(idError)
}
